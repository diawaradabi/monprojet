import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {AppareilService } from '../appareil.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-edit-appareil',
  templateUrl: './edit-appareil.component.html',
  styleUrls: ['./edit-appareil.component.scss']
})
export class EditAppareilComponent implements OnInit {
  defaultON='allumé';
  constructor(private addjout: AppareilService, private route : Router) { }

  ngOnInit() {
  }
  onSubmit(form:NgForm)
  {
    const name = form.value['name'];
    const status=form.value['status'];
    const describ=form.value['desciption'];
    this.addjout.addAppareil(name,status,describ);
    form.value['name']='';
    form.value['description']='';
    form.value[status]='Eteint';
    this.route.navigate(['/appareils']);

  }
}
