import { Component, Input, OnInit } from '@angular/core';
import { AppareilService } from '../appareil.service';
import { Router } from '@angular/router';
import { AppareilComponent } from '../appareil/appareil.component'

@Component({
  selector: 'app-list-appareil',
  templateUrl: './list-appareil.component.html',
  styleUrls: ['./list-appareil.component.scss']
})
export class ListAppareilComponent implements OnInit {
  appareils;
  isAuth = false;

  constructor(private appareil : AppareilService, private router: Router) 
  {  setTimeout(() => {
    this.isAuth=true;
  },1000);}

ngOnInit()
{
this.appareils = this.appareil.appareils;
}
Allumer()
{
  this.appareil.AllumerTout();
}

Eteindre()
{
  this.appareil.EteindreTout();
}

save()
{
  this.appareil.EnregistrerAppareil();
}

get()
{
  this.appareil.RecupererAppareil();
}
}
