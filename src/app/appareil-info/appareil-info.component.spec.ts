import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppareilInfoComponent } from './appareil-info.component';

describe('AppareilInfoComponent', () => {
  let component: AppareilInfoComponent;
  let fixture: ComponentFixture<AppareilInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppareilInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppareilInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
