import { Component, OnInit } from '@angular/core';
import { AppareilService } from '../appareil.service'
import { ActivatedRoute } from '@angular/router';
import { AppareilComponent } from '../appareil/appareil.component';

@Component({
  selector: 'app-appareil-info',
  templateUrl: './appareil-info.component.html',
  styleUrls: ['./appareil-info.component.scss']
})
export class AppareilInfoComponent implements OnInit {
app;

  constructor(private appareil : AppareilService, private route : ActivatedRoute) { }

  ngOnInit() {
   this.route.paramMap.subscribe(params => 
    {
     this.app = this.appareil.appareils[+params.get('id')];
    })
  }

}
