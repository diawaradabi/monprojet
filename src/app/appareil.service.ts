import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppareilService {

   appareilSubject=new Subject<any[]>();

  constructor(public httpclient:HttpClient) { }

  appareils=[

   { name:'Télévision',
     status : 'Allumé',
     Description : 'Ecran HD pour regarder les uniquement les matchs de foot !'
   },
   { name:'Ordinateur',
     status : 'Allumé',
     Description : 'Mon appareil préféré !'
   },
   { name:'Radio',
     status : 'Eteint',
     Description : 'Pour écouter des infos !'
   },
   { name:'Machine à lavée',
     status : 'Eteint',
     Description : 'Toujours des habits à lavés !'
   },

  ];

  getAppareil(id:number)
  {
    return this.appareils[id];
  }

  getAppareils()
  {
    return this.appareils;
  }

  AllumerTout()
  {
    for(let app of this.appareils)
    {
      app.status="Allumé"
    }
  }

  EteindreTout()
  {
    for(let app of this.appareils)
    {
      app.status="Eteint"
    }
  }
  eteindre(id:number)
  {
    this.appareils[id].status="Eteint"
  }

  Allumer(id:number)
  {
    this.appareils[id].status="Allumé"
  }

  Eteindre(id:number)
  {
    this.appareils[id].status="Eteint"
  }

  GetStatut(id:number)
  {
    return this.appareils[id].status;
  }
  
addAppareil(name:string,status:string, description:string)
{
  const objetappareil =
  {
    name: '',
    status: '',
    Description:''
  };
  objetappareil.name = name;
  objetappareil.status = status;
  objetappareil.Description=description;
  this.appareils.push(objetappareil);
}
  EnregistrerAppareil()
  {
    this.httpclient.put('https://back-end-cd1d9.firebaseio.com/appareils.json', this.appareils)
    .subscribe(
     () => {console.log("Finish")}
    
    )
  
  }

  RecupererAppareil()
  {
    this.httpclient.get<any[]>('https://back-end-cd1d9.firebaseio.com/appareils.json')
    .subscribe(
      (response) => {this.appareils=response}
    ),
    (error) =>{console.log("Erreur de récupération")}
  }
}
