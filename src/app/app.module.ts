import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MonPremierComponent } from './mon-premier/mon-premier.component';
import { AppareilComponent } from './appareil/appareil.component';
import { from } from 'rxjs';
import { RouterModule } from '@angular/router';
import { AppareilService } from './appareil.service';
import { AppareilInfoComponent } from './appareil-info/appareil-info.component';
import { ListAppareilComponent } from './list-appareil/list-appareil.component';
import { EditAppareilComponent } from './edit-appareil/edit-appareil.component';
import { AuthComponent } from './auth/auth.component'


@NgModule({
  declarations: [
    AppComponent,
    MonPremierComponent,
    AppareilComponent,
    AppareilInfoComponent,
    ListAppareilComponent,
    EditAppareilComponent,
    AuthComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot([

      { path: 'appareils', component:  ListAppareilComponent },
      { path: 'appareil/:id', component:  AppareilInfoComponent },
      { path: 'edit' , component: EditAppareilComponent},
      { path: '', component:  ListAppareilComponent },

    ])
  ],
  providers: [
    AppareilService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
