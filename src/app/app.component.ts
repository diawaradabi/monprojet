import { Component } from '@angular/core';
import { AppareilComponent } from './appareil/appareil.component';
import { AppareilService } from './appareil.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {  
}
