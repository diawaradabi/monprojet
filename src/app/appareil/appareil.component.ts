import { Component,Input, OnInit } from '@angular/core';
import{MonPremierComponent }from '../mon-premier/mon-premier.component' ;
import { AppareilService } from '../appareil.service'
import { Router } from '@angular/router'


@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {
  appareils;
  @Input() index:number;
  @Input() appareilName : string;
  @Input() appareilStatues : string;
  @Input() indexOfAppareil: number;

  constructor(private app : AppareilService, private router: Router) { }

  ngOnInit() {
    this.appareils=this.app.appareils;
  }

 getstatus()
  {
    return this.appareilStatues;
  }

  Allumer()
  {
    this.app.Allumer(this.indexOfAppareil);
  }
  Eteindre()
  {
    this.app.Eteindre(this.indexOfAppareil);
  }
  Statut()
  {
    return this.app.GetStatut(this.indexOfAppareil);
  }

  selectApp(app : string)
{
  let link = ['/appareil', app];
  this.router.navigate(link);
}
save()
{
  this.app.EnregistrerAppareil();
}

}
